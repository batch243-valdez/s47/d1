// console.log("Congrats!")

// Section: Document Object Model (DOM)
    // allows us to access or modify the properties of an  html element in a webpage
    // is it standar on how to get, change, add, or delete HTML elements
    // we will focus on use of DOM in managing forms.

// For selecting HTML elements we will be using document.querySelector
    // Syntax: document.querySelector("hmtl element")
    // the querySelector function takes a string input that is formatted like a css selector when applying the styles

    const txtFirstName = document.querySelector("#txt-first-name");
    // console.log(txtFirstName);

    const txtLastName = document.querySelector("#txt-last-name")

    const name = document.querySelectorAll(".full-name");
    // console.log(name);

    const span = document.querySelectorAll("span");
    // console.log(span)

    const text = document.querySelectorAll("input[type]")
    // console.log(text)

    const spanFullName = document.querySelector("#fullName")

// Section: Event Listeners
    // whenever a user interacts with a webpage, this action is considered as an event.
    // working with events is large part of creating interactivity in a webpage
    // specific function that perform an action

// The function use is addEventListener, it takes two arguments
    // first argument a string identifying an event
    // second argument, fuction that the listener will trigger once the "specified event" is triggered.

    txtFirstName.addEventListener("keydown", (event) => {
        console.log(event.target.value);
    })

    const fullName = () => {
        spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
    }

    txtFirstName.addEventListener("keyup", fullName);
    txtLastName.addEventListener("keyup", fullName);


    textToChange = document.getElementById("textColor");

    textColor.addEventListener("change", (event) => {
        document.getElementById("fullName").style.color = event.target.value
    })
